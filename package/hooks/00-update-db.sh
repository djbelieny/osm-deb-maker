#!/bin/bash

#
#	This is an example of a hook that is run during
# package setup and before package creation in 
# order to tag sql files with the correct version
#

echo -ne 'Tagging database changes file .............................\r'
cd $deb_package_source_path
cat data/update.sql > data/sqlupdate/update_$deb_package_revision.sql
echo "" > data/update.sql
git add data/update.sql data/sqlupdate/update_$deb_package_revision.sql > /dev/null 2>&1
git commit -m "Tagging database changes for package revision $deb_package_revision" > /dev/null 2>&1
cd - > /dev/null 2>&1
echo -ne 'Tagging database changes file ............................. [OK]\r\n'

exit 0

##END OF HOOK
