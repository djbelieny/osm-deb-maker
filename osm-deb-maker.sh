#!/bin/bash
#
# INFO: http://unix.stackexchange.com/questions/30303/how-to-create-a-deb-file-manually
#
#	osm-deb-maker.sh
# developed by Open Source Mind LLC
#

function show_help {
	echo "------------------------------------------------------------------------------"
	echo "Open Source Mind - debian package maker v.2.0.0                               "
	echo "------------------------------------------------------------------------------"
	
	if [ "$GOODTOGO" = "NO" ] ; then
		echo " "
		echo " ****** $ERRORMSG"
		echo " "
	fi
	
	echo "-h or  --help		           Show this help message                                 "
	echo "-c [FILE] or --config      [FILE]	 Name of the config file for the debian package	"
	echo "-b [PATH] or --build-path  [PATH]	 Directory containing build files, scripts and  "
  echo "                                   and other info used to build package.          "
	echo "-s [PATH] or --source-path [PATH]	 Directory containing files to be packaged	    "
	echo "--------------------------------------------------------------------------------- "
	exit 0
}

function normalize_path()
{
    # Remove all /./ sequences.
    local   path=${1//\/.\//\/}
    
    # Remove dir/.. sequences.
    while [[ $path =~ ([^/][^/]*/\.\./) ]]
    do
        path=${path/${BASH_REMATCH[0]}/}
    done
		
    echo ${path%/}
}

# Check for command line parameters

HELP="NO"
while [[ $# > 0 ]]
do
key="$1"
shift

case $key in
    -c|--config)
    deb_package_config_file="$1"
    shift
    ;;
    -s|--source-path)
    deb_package_source_path="$1"
    shift
    ;;
	  -b|--build-path)
    deb_package_build_path="$1"
    shift
    ;;
    -h|--help)
		HELP="YES"
    ;;
    *)
    # unknown option
		HELP="YES"
    ;;
esac
done

if [ "$HELP" = "YES" ] ; then
	  show_help
fi

GOODTOGO="YES"

if [ ! -n "$deb_package_config_file" ] ; then
	GOODTOGO="NO"
	ERRORMSG="Invalid Syntax: package config file required."
	show_help
fi

if [ ! -n "$deb_package_source_path" ] ; then
	GOODTOGO="NO"
	ERRORMSG="Invalid Syntax: package source path required."
	show_help
fi

if [ ! -d "$deb_package_source_path" ] ; then
	GOODTOGO="NO"
	ERRORMSG="Directory not found: package source path not found."                                          
	show_help
fi

if [ ! -d "$deb_package_build_path" ] ; then
	GOODTOGO="NO"
	ERRORMSG="Directory not found: package build path not found."                                          
	show_help
fi

# Ckeck for package configuration file

if [ ! -f "$deb_package_config_file" ] ; then
	GOODTOGO="NO"
	ERRORMSG="File not found: package config file not found."                                          
	show_help
fi

# Source configuration
source $deb_package_config_file

# Check for package configuration sanity

if [ "$deb_package_long_name" = "" ] ; then
	  echo "Invalid package configuration: Long package name not found"
		exit
fi

if [ "$deb_package_short_name" = "" ] ; then
	  echo "Invalid package configuration: Short package name not found"
		exit
fi

if [ "$deb_package_maintainer" = "" ] ; then
	  echo "Invalid package configuration: Package author not found"
		exit
fi

if [ "$deb_package_arch" = "" ] ; then
	  echo "Invalid package configuration: Package architecture not found"
		exit
fi

if [ "$deb_package_priority" = "" ] ; then
	  echo "Invalid package configuration: Package priority not found"
		exit
fi

if [ "$deb_package_section" = "" ] ; then
	  echo "Invalid package configuration: Package section not found"
		exit
fi

if [ "$deb_package_version" = "" ] ; then
	  echo "Invalid package configuration: Package version not found"
		exit
fi

if [ "$deb_package_description" = "" ] ; then
	  echo "Invalid package configuration: Package description not found"
		exit
fi

if [ "$deb_package_install_path" = "" ] ; then
	  echo "Invalid package configuration: Package install path not found"
		exit
fi


# Sanitize paths

export deb_package_source_path=$(normalize_path $deb_package_source_path)
export deb_package_build_path=$(normalize_path $deb_package_build_path)

deb_package_install_path=$(normalize_path $deb_package_install_path)
export deb_package_install_path=${deb_package_install_path#"/"}

# Prepare package source tree
echo -ne 'Preparing package source ..................................\r'
rm -rf debian-binary control.tar.gz data.tar.gz
rm -rf $deb_package_build_path/src/*
rm -rf $deb_package_build_path/$deb_package_short_name-$deb_package_version-$deb_package_arch.deb
rm -rf $deb_package_short_name-$deb_package_version-$deb_package_arch.deb
mkdir -p $deb_package_build_path/src/$deb_package_install_path
echo -ne 'Preparing package source .................................. [OK]\r\n'
echo -ne 'Copying source files ...................................... \r'
rsync -ah $deb_package_source_path/* $deb_package_build_path/src/$deb_package_install_path
echo -ne 'Copying source files ...................................... [OK]\r\n'
echo -ne 'Processing exclude rule ................................... \r'
# Process excludes
EXCLUDE_ARR=$(echo ${deb_package_excludes})
OIFS=$IFS;
IFS=",";
for e in ${EXCLUDE_ARR[@]} ;  do
	rm -rf $deb_package_build_path/src/$deb_package_install_path/${e}
done
IFS=$OIFS;

echo -ne 'Processing exclude rules .................................. [OK]\r\n'
echo -ne 'Running setup hooks  ...................................... \r'

for HOOK in $deb_package_build_path/hooks/* ; do
	if [ -x $HOOK ] ; then
			$HOOK
	fi
done

echo -ne 'Running setup hooks  ...................................... [OK]\r\n'

# Prepare package descriptors
echo -ne 'Preparing DEBIAN control files ............................ \r'
mkdir -p $deb_package_build_path/src/DEBIAN
# create control file
echo "Package: $deb_package_short_name" > $deb_package_build_path/src/DEBIAN/control
echo "Priority: $deb_package_priority" >> $deb_package_build_path/src/DEBIAN/control
echo "Section: $deb_package_section" >> $deb_package_build_path/src/DEBIAN/control
echo "Maintainer: $deb_package_maintainer" >> $deb_package_build_path/src/DEBIAN/control
echo "Architecture: $deb_package_arch" >> $deb_package_build_path/src/DEBIAN/control
echo "Version: $deb_package_version-$deb_package_revision" >> $deb_package_build_path/src/DEBIAN/control
echo "Provides: $deb_package_short_name+$deb_package_version" >> $deb_package_build_path/src/DEBIAN/control
if [ "${#deb_package_depends}" -gt "0" ] ;then
	echo "Depends: ${deb_package_depends}" >> $deb_package_build_path/src/DEBIAN/control
fi
echo "Description: $deb_package_description" >> $deb_package_build_path/src/DEBIAN/control
cp $deb_package_build_path/deb-scripts/* $deb_package_build_path/src/DEBIAN/
chmod -R 755 $deb_package_build_path/src/DEBIAN/*
echo -ne 'Preparing DEBIAN control files ............................ [OK]\r\n'

echo -ne 'Building package .......................................... \r'
# Create package
cd $deb_package_build_path/src
tar czf $deb_package_build_path/data.tar.gz [a-z]*
cd DEBIAN
tar czf $deb_package_build_path/control.tar.gz *
cd $deb_package_build_path
echo 2.0 > debian-binary
ar r $deb_package_short_name-$deb_package_version-$deb_package_revision-$deb_package_arch.deb debian-binary control.tar.gz data.tar.gz
echo -ne 'Building package .......................................... [OK]\r\n'

# Cleaning up
echo -ne 'Cleaning up ............................................... \r'
rm -rf $deb_package_build_path/debian-binary $deb_package_build_path/control.tar.gz $deb_package_build_path/data.tar.gz
rm -rf $deb_package_build_path/src/*
echo -ne 'Cleaning up ............................................... [OK]\r\n'

## END
