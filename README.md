#
# osm-deb-maker.sh
# author: Development Team @ Opens Source Mind


This script helps you build debian packages on 
'nix based systems without the need for debian
packaging tools.

In order to build a package you must first
create a setup.conf file in the package directory
which will contain all the necessary information
for the build.

If you want to include scripts to the packages you
may do so by adding or editing the scripts in the
deb-scripts folder. We provide you with samples
for preinst, prerm, postinst and postrm scripts.

More info available at the command line by
entering: osm-deb-maker.sh --help

Installing and Using:
```
wget -O osm-deb-maker.sh https://bitbucket.org/djbelieny/osm-deb-maker/raw/master/osm-deb-maker.sh
```
or
```
curl -o osm-deb-maker.sh https://bitbucket.org/djbelieny/osm-deb-maker/raw/master/osm-deb-maker.sh
```
